<?xml version ="1.0" encoding="UTF-8"?>
<xsl:stylesheet version = "2.0" xmlns:xsl ="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>Guitar Solos in XML</title>
                <link rel="stylesheet" href="main.css"></link>
                <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"></link>

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"></link>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

            </head>
            <body>
                                <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="">Guitar Solos XML</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="./smellsLikeTeenSpirit.xml">Smells Like Teen Spirit</a></li>
      <li><a href="./stairwayToHeaven.xml"> Stairway to Heaven</a></li>
    </ul>
  </div>
</nav>
<br/>
                <h1>Guitar Solos in XML</h1>
                <a href="./smellsLikeTeenSpirit.xml">Smells Like Teen Spirit - Nirvana; Guitarist: Kurt Cobain</a>
                <br></br><br></br>

                <a href="./stairwayToHeaven.xml">Stiarway to Heaven - Led Zeppelin; Guitarist: Jimmy Page</a>
                <br/><br/>
                                <a target="_blank" href="https://www.soundslice.com/musicxml-viewer/">Soundsplice - This tool allows you to upload MusicXML and get sheet music in return.</a>
                <br/><br/>
                <a href="./dtd_1.dtd" download="MusixXML_DTD.dtd">Download DTD</a>
                <br/><br/>
                <a href="./schema_1.xsd" download="MusixXML_Schema.xsd">Download Schema</a>
                <h2>Smells Like Teen Spirit - Solo Written and Performed by Kurt Cobain</h2>



                <a href="./smellsLikeTeenSpirit_raw.xml">View Raw XML Data - XML written by Matt Weiner</a>

                <br/><br/>

                <a href="./smellsLikeTeenSpirit_stats.xml">View Statistics (Computational Stylesheet)</a>
                <br/>
                <!-- Sheet Music -->
                <img src="teenSpirit.png" ></img>

                

            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>