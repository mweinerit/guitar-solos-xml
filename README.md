# Guitar Solos XML
README

For instructions on how to view the project, please see instructions.txt

The goal is to leverage MusicXML to tab tab out 2 guitar solos using MusicXML.  This project will be done iteratively, which means that a base project will be laid out and then each part will be improved over and over again as time permits.
Accomplished.

The project must accomplish these items:
Create an XML-based Rich Website (at least a page or two of it!)
Accomplished.

The site must have two or more collections of data that are displayed. The data displayed must be stored in XML format.
Accomplished

Progress: 2 xml files have been started.  Smells Like Teen Spirit and Stairway to Heaven Solos.  The pages now are linked togeter and xslt has been used to pull in image of the tablature sheet music.  Both xml data sets have been improved many times for accuracy and more work is to come.

The site must use a stylesheet to style the elements of the site
Accomplished.

Progress:  Added basic css style sheet and linked in Bootstrap css to leverage

The site must feature one page which uses a computational stylesheet to display statistics related to at least one of the collections of data.
Accomplished.

Progress:  A stats page was created for each song showing the number of notes in the xml solo.

All stylesheets must be external to the document
Accomplished.

Progress:  All stylesheets are external and boostrap is pulled in through a cdn


No use of formatting tags (e.g., <B> or <FONT>) within XHTML are allowed. All formatting must be by CSS or XSL.
Accomplished.