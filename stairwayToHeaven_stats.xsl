<?xml version ="1.0" encoding="UTF-8"?>
<xsl:stylesheet version = "2.0" xmlns:xsl ="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>Guitar Solos in XML</title>
                <link rel="stylesheet" href="main.css"></link>
                <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"></link>

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"></link>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

            </head>
            <body>
                <h1>Guitar Solo Statistics (Computational Style Sheet) - Stairway XML</h1>
                <a href="./smellsLikeTeenSpirit.xml">Back to Home Page</a>
                <br></br><br></br>



                <h2>Statistics</h2>
                <h3>Number of Notes in Solo:
                <xsl:for-each select="//part">
                 <xsl:value-of select="count(//note)"/>
                </xsl:for-each>
                </h3>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>